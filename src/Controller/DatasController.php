<?php

class Datas extends Controller {

    /**
     * Fonction de logging Git
     *
     */
    function execPrint($command, $fileName) {
        $result = array();
        exec($command, $result);
        $fileContent = "##### $command \n";
        foreach ($result as $line) {
            $fileContent .= "$line \n";
        }
        file_put_contents($fileName, $fileContent);
    }

    /**
     * Fonction de creation Git
     *
     */
    function createFredOSDatas() {
        $old_path = getcwd();
        chdir(ROOT.'public/');
        $this->execPrint('git clone https://oauth2:'.GIT_TOKEN.'@gitlab.com/'.GIT_REPO.'/', ROOT.'logs/git_clone.log');
        chdir($old_path);
    }

    /**
     * Fonction de mise à jour Git
     *
     */
    function updateFredOSDatas() {
        $old_path = getcwd();
        chdir(ROOT.'public/fredos-datas/');
        $this->execPrint('git remote set-url origin https://oauth2:'.GIT_TOKEN.'@gitlab.com/'.GIT_REPO.'/', ROOT.'logs/git_remote.log');
        $this->execPrint("git status", ROOT.'logs/git_status.log');
        $this->execPrint("git reset --hard HEAD", ROOT.'logs/git_reset.log');
        $this->execPrint("git pull", ROOT.'logs/git_pull.log');
        chdir($old_path);
    }

    /**
     * Vue datas index
     *
     */
    public function index() {

        $page = array(
            'title' => "FredOS Datas",
            'incipit' => "FredOS Datas.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
        );
        $this->render('main', compact('page'));
    }

    /**
     * Vue datas linux desktop
     *
     */
    public function linux_desktop() {
        if (GIT_TOKEN != "glpat-xxxxx") {
            if (file_exists(ROOT.'logs/git_clone.log') === false) { $this->createFredOSDatas(); }
            $this->updateFredOSDatas();
            $file = file_get_contents(ROOT."public/fredos-datas/datas/linux/desktop.json");
        } else {
            $file = file_get_contents(ROOT."webroot/datas/linux/desktop.json");
        }
        $page = array(
            'title' => "FredOS Datas linux desktop",
            'incipit' => "FredOS Datas linux desktop.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'content' => $file,
            'format' => "json"
        );
        // Catch cURL/Wget requests
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/^(curl|wget)/i', $_SERVER['HTTP_USER_AGENT'])) {
            $this->render('json', compact('page'));
        } else {
            $this->render('html', compact('page'));
        }
    }

    /**
     * Vue datas linux server
     *
     */
    public function linux_server() {
        if (GIT_TOKEN != "glpat-xxxxx") {
            if (file_exists(ROOT.'logs/git_clone.log') === false) { $this->createFredOSDatas(); }
            $this->updateFredOSDatas();
            $file = file_get_contents(ROOT."public/fredos-datas/datas/linux/server.json");
        } else {
            $file = file_get_contents(ROOT."webroot/datas/linux/server.json");
        }
        $page = array(
            'title' => "FredOS Datas linux server",
            'incipit' => "FredOS Datas linux server.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'content' => $file,
            'format' => "json"
        );
        // Catch cURL/Wget requests
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/^(curl|wget)/i', $_SERVER['HTTP_USER_AGENT'])) {
            $this->render('json', compact('page'));
        } else {
            $this->render('html', compact('page'));
        }
    }

    /**
     * Vue datas windows groups
     *
     */
    public function windows_groups() {
        if (GIT_TOKEN != "glpat-xxxxx") {
            if (file_exists(ROOT.'logs/git_clone.log') === false) { $this->createFredOSDatas(); }
            $this->updateFredOSDatas();
            $file = file_get_contents(ROOT."public/fredos-datas/datas/windows/groups.csv");
        } else {
            $file = file_get_contents(ROOT."webroot/datas/windows/groups.csv");
        }
        $page = array(
            'title' => "FredOS Datas windows groups",
            'incipit' => "FredOS Datas windows groups.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'content' => $file,
            'format' => "csv"
        );
        // Catch cURL/Wget requests
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(curl|wget|PowerShell)/i', $_SERVER['HTTP_USER_AGENT'])) {
            $this->render('csv', compact('page'));
        } else {
            $this->render('html', compact('page'));
        }
    }

    /**
     * Vue datas windows ous
     *
     */
    public function windows_ous() {
        if (GIT_TOKEN != "glpat-xxxxx") {
            if (file_exists(ROOT.'logs/git_clone.log') === false) { $this->createFredOSDatas(); }
            $this->updateFredOSDatas();
            $file = file_get_contents(ROOT."public/fredos-datas/datas/windows/ous.csv");
        } else {
            $file = file_get_contents(ROOT."webroot/datas/windows/ous.csv");
        }
        $page = array(
            'title' => "FredOS Datas windows ous",
            'incipit' => "FredOS Datas windows ous.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'content' => $file,
            'format' => "csv"
        );
        // Catch cURL/Wget requests
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(curl|wget|PowerShell)/i', $_SERVER['HTTP_USER_AGENT'])) {
            $this->render('csv', compact('page'));
        } else {
            $this->render('html', compact('page'));
        }
    }

    /**
     * Vue datas windows users
     *
     */
    public function windows_users() {
        if (GIT_TOKEN != "glpat-xxxxx") {
            if (file_exists(ROOT.'logs/git_clone.log') === false) { $this->createFredOSDatas(); }
            $this->updateFredOSDatas();
            $file = file_get_contents(ROOT."public/fredos-datas/datas/windows/users.csv");
        } else {
            $file = file_get_contents(ROOT."webroot/datas/windows/users.csv");
        }
        $page = array(
            'title' => "FredOS Datas windows users",
            'incipit' => "FredOS Datas windows users.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'content' => $file,
            'format' => "csv"
        );
        // Catch cURL/Wget requests
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(curl|wget|PowerShell)/i', $_SERVER['HTTP_USER_AGENT'])) {
            $this->render('csv', compact('page'));
        } else {
            $this->render('html', compact('page'));
        }
    }

}
<?php

class Infos extends Controller {
    
    /**
     * Vue php informations
     *
     */
    public function index() {

        $page = array(
            'title' => "Infos Index",
            'incipit' => "PHP Informations.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
        );
        $this->render('main', compact('page'));
    }

    /**
     * Vue php informations extensions
     *
     */
    public function extensions() {

        $page = array(
            'title' => "Infos Extensions",
            'incipit' => "PHP Extensions.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
        );
        $this->render('main', compact('page'));
    }

    /**
     * Vue php imagemagick
     *
     */
    public function magick() {

        $page = array(
            'title' => "Infos ImageMagick",
            'incipit' => "PHP ImageMagick.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
        );
        $this->render('main', compact('page'));
    }

}
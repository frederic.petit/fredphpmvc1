<?php

abstract class Controller {

    /**
     * Afficher une vue
     * On démarre le buffer de sortie, on génère la vue, on stocke le contenu dans $content puis on fabrique le "template"
     *
     * @param string $file
     * @param array $data
     * @return void
     */
    public function render(string $file, array $data = []) {
        extract($data);
        ob_start();
        require_once(ROOT.'templates/'.ucfirst(get_class($this)).'/'.$file.'.php');
        $content = ob_get_clean();
        if ($file == "csv") {
            require_once(ROOT.'templates/layout/csv.php');
        } elseif ($file == "json") {
            require_once(ROOT.'templates/layout/json.php');
        } else {
            require_once(ROOT.'templates/layout/html.php');
        }
    }

}
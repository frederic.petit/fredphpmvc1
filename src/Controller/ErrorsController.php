<?php

class Errors extends Controller {

    /**
     * Vue error 404
     *
     */
    public function error(int $error = 0, string $origin = "") {

        // define error's url
        $url = mb_strimwidth($_SERVER['REQUEST_URI'], 0, 45, "...");

        // define error's reason
        if ((isset($origin)) && ($origin != "")) {
            $originText = mb_strimwidth($origin, 0, 35, "...");
        }
        switch ($error) {
            case 0:
                $reason = "generic error"; break;
            case 1:
                $reason = "Action $originText not exists"; break;
            case 2:
                $reason = "Controller $originText's file not exists"; break;
            case 3:
                $reason = "Controller $originText not exists"; break;
            case 4:
                $reason = "URL not exists in this MVC arch"; break;
            case 10:
                $reason = "'//' not allow in URL"; break;
        }

        $page = array(
            'title' => "Error",
            'incipit' => "Page unavailable.",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'url' => "$url",
            'reason' => "$reason"
        );
        $this->render('main', compact('page'));

    }

}
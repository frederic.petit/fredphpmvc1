//############################
//#  ______            _ _____  _    _ _____  __  ____      _______ 
//# |  ____|          | |  __ \| |  | |  __ \|  \/  \ \    / / ____|
//# | |__ _ __ ___  __| | |__) | |__| | |__) | \  / |\ \  / / |     
//# |  __| '__/ _ \/ _` |  ___/|  __  |  ___/| |\/| | \ \/ /| |     
//# | |  | | |  __/ (_| | |    | |  | | |    | |  | |  \  / | |____ 
//# |_|  |_|  \___|\__,_|_|    |_|  |_|_|    |_|  |_|   \/   \_____|
//#                                                                 
//# by : fredericpetit.fr
//# last revision : 21/06/2022
//############################
//# script : main.js
//# usage : main script
//############################

// #################### dark mode ####################
// src : https://css-tricks.com/a-complete-guide-to-dark-mode-on-the-web/#combining
function changeClass(element, oldClass, newClass) {
    state = document.getElementById(element);    
    state.classList.remove(oldClass);
    state.classList.add(newClass);
}

const btn = document.querySelector("#btn-toggle");
const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");
// current theme
const currentTheme = localStorage.getItem("theme");
if (currentTheme == "dark") {
    console.log("theme initiated to dark.");
    document.body.classList.toggle("dark-theme");
    changeClass("icon-toggle", "fa-spinner", "fa-moon");
} else if (currentTheme == "light") {
    console.log("theme initiated to light.");
    document.body.classList.toggle("light-theme");
    changeClass("icon-toggle", "fa-spinner", "fa-sun");
} else {
    console.log("theme not previously defined.");
    changeClass("icon-toggle", "fa-spinner", "fa-sun");
}
// dark mode button
btn.addEventListener("click", function () {
    // toggles icon
    var stateLoaded = document.getElementById("icon-toggle");
    var loadedTheme = stateLoaded.classList.contains("fa-sun")
    ? "light"
    : "dark";
    if (loadedTheme == "light") {
        console.log("theme changed to dark.");
        changeClass("icon-toggle", "fa-sun", "fa-moon");
    } else if (loadedTheme == "dark") {
        console.log("theme changed to light.");
        changeClass("icon-toggle", "fa-moon", "fa-sun");
    }
    // toggles theme
    if (prefersDarkScheme.matches) {
        document.body.classList.toggle("light-theme");
        var theme = document.body.classList.contains("light-theme")
        ? "light"
        : "dark";
    } else {
        document.body.classList.toggle("dark-theme");
        var theme = document.body.classList.contains("dark-theme")
        ? "dark"
        : "light";
    }
    localStorage.setItem("theme", theme);
});
<h2><?= $page['incipit'] ?></h2>

<?php

function do__error($action, $reason) {
    echo "<b>reason : <i>".$reason.".</i></b>";
}

// dispatcher
if ($page['action'] == "error") {
    do__error($page['action'], $page['reason']);
}

?>

<script type="text/JavaScript">
    setTimeout("location.href = '<?= WEBROOT ?>';", <?= TIMEOUT ?>);
</script>
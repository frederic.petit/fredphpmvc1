<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="<?= APPNAME ?> <?= VERSION ?>">
    <title><?= APPNAME ?> :: <?= $page['title'] ?></title>
    <link rel="icon" href="<?= WEBROOT ?>webroot/favicon.ico" />
    <!-- natives -->
    <link href="<?= WEBROOT ?>webroot/css/main.css" rel="stylesheet" />
    <link href="<?= WEBROOT ?>webroot/css/fonts.css" rel="stylesheet" />
    <!-- Bootstrap & Font Awesome CSS -->
    <link href="<?= WEBROOT ?>public/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= WEBROOT ?>public/components/font-awesome/css/all.min.css" rel="stylesheet" />
</head>
<body>

<!-- #################### nav #################### -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?= WEBROOT ?>"><?= APPNAME ?> (<?= VERSION ?>)</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav me-auto mb-2 mb-md-0"><?php
            // current controller
            $current_controller = strtolower($page['controller']);
            $array_controllers = array_diff(scandir(ROOT.'src/Controller'), array('.', '..', 'index.html', 'AppController.php', 'ErrorsController.php'));
            // resort controllers
            function resort_controllers (&$array, $value) {
                $key = array_search($value, $array);
                if($key) unset($array[$key]);
                array_unshift($array, $value);  
                return $array;
            }
            resort_controllers($array_controllers, "MainController.php");
            // loop controllers
            foreach ($array_controllers as &$value) {
                $value_str1 = str_replace('Controller.php', '', $value);
                $value_str2 = strtolower(str_replace('Controller.php', '', $value));
                echo "<li class=\"nav-item\"><a class=\"nav-link".($current_controller == $value_str2 ? ' active' : '')."\" aria-current=\"".($current_controller == $value_str2 ? ' active' : '')."\" href=\"".WEBROOT."index.php?p=$value_str2/index\" tabindex=\"0\" title=\"$value_str1/Index\">$value_str1</a></li>";
            }
        ?></ul>
    </div>
    </div>
</nav>

<!-- #################### main #################### -->
<main>
    <div class="container py-4">
        <div id="splash__main" class="p-1 mb-1 bg-light rounded-3">
            <div class="container-fluid py-3">
                <div id="cd">
                    <span id="cd__mark">></span>
                    <span id="cd__text">$ cd ~<?= WEBROOT ?><?= $current_controller ?></span>
                    <span id="cd__cursor"></span>
                </div>
                <!-- main part -->
                <div id="content">
                    <?= $content ?>
                </div>
                <!-- trace -->
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Stack Trace</button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <?php if (TRACE === true) { echo "<code>"; print_r($_SERVER); echo "</code>"; } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->
        <footer class="pt-3 mt-4 text-muted border-top">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <p><?= APP_FOOTER ?></p>
                    </div>
                    <div class="col-sm-auto">
                        <p><button type="button" id="btn-toggle" class="btn btn-primary"><i id="icon-toggle" class="fa-solid fa-spinner"></i></button></p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</main>

<!-- natives -->
<script src="<?= WEBROOT ?>webroot/js/main.js"></script>
<!-- Bootstrap JS -->
<script src="<?= WEBROOT ?>public/twbs/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>
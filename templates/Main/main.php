<h2><?= $page['incipit'] ?></h2>

<h3>Liste des controllers :</h3>
<ul>
    <?php
        $files = array_diff(scandir(ROOT.'src/Controller'), array('.', '..', 'index.html', 'AppController.php', 'ErrorsController.php'));
        foreach ($files as &$value) {
            $value_str1 = str_replace('Controller.php', '', $value);
            $value_str2 = strtolower(str_replace('Controller.php', '', $value));
            echo "<li><a href=\"".WEBROOT."index.php?p=$value_str2\" title=\"$value_str1 Controller\">$value_str1</a></li>";
        }
    ?>
</ul>